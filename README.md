## Shell

Personal shell environment. Settings must be passed through env file and passed to the docker run command:

  `docker run --rm -it --env-file ~/env -v /home:/home shell`

**Accepted variables for env file**

* `UID` User ID
* `USER` Username


### Tools Installed

* git 
* iputils-ping
* vim
* tmux
* screen
* wget
* tzdata
* net-tools
* curl
