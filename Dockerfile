FROM debian:stretch

#configure locales

ENV LANGUAGE=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
RUN apt update && apt install locales -y 
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
  dpkg-reconfigure --frontend=noninteractive locales && \
  update-locale LANG=en_US.UTF-8 

# apt packages

RUN apt update && apt install -y  \
  git \
  iputils-ping \
  vim \
  tmux \
  screen \
  wget \
  tzdata \
  net-tools \
  zsh \
  yadm \
  ipmitool \
  mosh \
  curl

RUN curl -fsSL get.docker.com -o /tmp/get-docker.sh && \
  sh /tmp/get-docker.sh \
  apt update && apt install docker-ce -y

RUN apt install sudo -y
COPY shell.sh /usr/local/bin/shell.sh
CMD ["/usr/local/bin/shell.sh"]
