#!/bin/bash

PASS=$(perl -e 'print crypt($ARGV[0],"password")' $PASSWORD)
useradd -p $PASS -u $UID -ms /usr/bin/zsh $USER
usermod -aG sudo $USER
echo $HOSTNAME > /etc/hostname
su -c "/usr/bin/tmux" $USER
